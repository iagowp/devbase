import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from './containers/home';
import User from './containers/user';
import './App.css';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/:userId" component={User} />
      </Switch>
    </Router>
  );
}

export default App;
