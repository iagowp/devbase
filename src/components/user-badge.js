import { Link } from "react-router-dom";


export default function UserBadge ({user}) {
  return (
    <Link to={`/${user}`}>
      <div className='user-badge'>
        {user}
      </div>
    </Link>
  );
}