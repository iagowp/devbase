import { Link } from "react-router-dom";

export default function Menu ({isHome = true}) {
  return (
    <div className='menu'>
      {isHome
        ? null 
        : <Link to='/' className='back'> Back </Link>
      }
      Home
    </div>
  )
}
