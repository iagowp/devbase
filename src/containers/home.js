import Menu from "../components/menu";
import UserBadge from "../components/user-badge";

const users = ["GrahamCampbell", "fabpot", "weierophinney", "rkh", "josh"];

export default function Home () {
  return (
    <div>
      <Menu />
      <div className='container'>
        <h2>Top5 GitHub users</h2>
        <p>Tap the username to see more information</p>
        <div className='users'>
          {users.map(u => (
            <UserBadge user={u} key={u} />
          ))}
        </div>
      </div>
    </div>
  )
}