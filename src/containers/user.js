import { useEffect, useState } from "react";
import { useParams } from "react-router";
import Menu from "../components/menu";

export default function User () {
  const { userId } = useParams();

  const [user, setUser] = useState(null);

  useEffect(() => {
    const getUser = async () => {
      const response = await fetch(`https://api.github.com/users/${userId}`);
      const body = await response.json();
      setUser(body);
    };

    getUser();
  }, [userId]);

  // todo: this should be a spinner component inside the render, as menu shouldnt disappear
  if (!user) return 'Loading';

  return (
    <div>
      <Menu isHome={false} />
      <div className='container'>
        <img src={user.avatar_url} className='avatar' alt={`Avatar from ${userId}`} />
        <h4>{user.name}</h4>
        <div>{user.location}</div>
        <hr />
      </div>
    </div>
  )
}